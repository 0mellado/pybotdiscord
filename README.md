# Bot de discord interprete de python

## Instalación:

### Descargar el repositorio:

```
git clone https://gitlab.com/0mellado/pybotdiscord.git
```

### Instalar la librería necesaria:

```
python3 -m pip install -U discord.py
```

### Crear el archivo de configuración:

```
vim config.json
```
Dentro del archivo va el prefijo y el token del bot.
```
{
    "prefix": "-",
    "token": "aqui-va-el-token-del-bot" 
}
```

## Uso:

Para encender el bot hay que ejecutar el archivo index.py. Puede ser con el interprete desde la terminal o
con los permisos de ejecución, ya que en el inicio del archivo index se define con que se va a ejecución.

```python
python3 index.py
```
Para cambiar los permisos se debe usar con el comando:
```
chmod 744 index.py
```
Y se puede ejecutar:
```python
./index.py
```


