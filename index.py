#!/usr/bin/python3
import os
import json
import discord
import random
from discord import Embed, File
from discord.ext import commands
from datetime import datetime

with open("./config.json", "r") as f:
    data = json.load(f)
    prefix = data["prefix"]
    token = data["token"]

bot = commands.Bot(command_prefix=prefix)

def createFile(msg, name, mode):
    file = open(name, mode)
    cmd = "chmod +x " + name
    os.system(cmd)
    file.write("#!/usr/bin/python3" + os.linesep)
    file.write(msg)
    file.close()

def readFileRes():
    file = open("./response", "r")
    response = file.read()

    return response

def comandoHelp():
    file = File("/home/oscar/pyBotDiscord/help1.png",
                        filename="image.png")
    embedHelp = Embed(
        title="Uso:",
        color=discord.Color.from_rgb(
            random.randint(0,255),
            random.randint(0,255),
            random.randint(0,255))
    )
    embedHelp.set_image(url="attachment://image.png")

    return embedHelp, file

@bot.event
async def on_ready():
    print("El bot esta corriendo ...")

@bot.event
async def on_message(message):

    hour = datetime.now()
    if not message.author.bot:
        if message.content == '-hola':
            if hour.hour < 12:
                await message.channel.send('Buenos días', reference=message)
            else:
                await message.channel.send('buenas tarde', reference=message)

        if message.content == "-run" or message.content == "-help":
            embedHelp = comandoHelp()
            await message.channel.send(embed=embedHelp[0], file=embedHelp[1])

        elif message.content.startswith("-run"):
            msg = message.content
            msg = msg.replace("`", "").replace("python", "").replace("-run", "")

            createFile(msg, "code.py", "w")
            cmd = "./code.py > response"

            try:
                os.system(cmd)
                response = readFileRes()
            except:
                print("hola error")

            await message.channel.send(f"```\n{response}\n```",
                                       reference=message)

            os.system("rm -rf ./code.py")

    else:
        return

bot.run(token)
